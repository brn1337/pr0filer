# Index

[[_TOC_]]

# Overview

Pr0filer is a reconnaissance tool for browsers, implemented in JS and PHP. 
Its purpose is to gather information from the victim for further targeted attacks.

```mermaid
flowchart TB
    A((Victim)) -->|1. Open| B{{Website containing pr0filer}}
    B --> |2. request provider.js| C(provider.php)
    C -->|3. Return appropiate js agent| G{{js agent}}
    G --> |4. Collect data| G
    G --> |5. Send collected data| D(receiver.php)
    G --> |5.2 if IE: reopen itself in EDGE | B
    D --> |6. Save data on disk| E[(log.txt)]
    G -.-> |3.1 fa:fa-exclamation Anomaly| F[(anomalies.txt)]
    C -.-> |2.1 fa:fa-exclamation Anomaly| F
    D -.-> |5.1 fa:fa-exclamation Anomaly| F
    F --> H((Exit))
    E --> |7 optional: Open URI scheme/redirect| H
    subgraph C2
    C
    D
    E
    F
    end
    subgraph Victim's Browser
    direction TB
    B
    G
    end
```

# Features

Supported browsers:
- Chrome
- Firefox
- Edge
- IE (ad-hoc version)
- most modern browsers

Collected data:
- Everything inside navigator
- GPU information
- Blocked website [Not supported by IE]
- Antivirus/EDR (to be improved, currently "Bitdefender Wallet" browser extension is being detected) [Not supported by IE]
- Screen and window size
- WebRTC IP detection [Not supported by IE]
- Touchscreen support

IMPORTANT: the IE version by default will also open a window of itself in edge to collect more info

Exploitation:
- Open an office uri at the end of the data collection (in IE this will happen without a prompt in my tests)

NOTE: the uri can open a malicious file (ex. .xlsxm) or being the exploit itself (ex. https://positive.security/blog/ms-officecmd-rce, CVE-2020-13699)

Deception:
- Block Proxy/VPN/Tor/Relay
- Block bots/crawlers/search engines
- Block access to all unintended resources
- C2 traffic heavily obfuscated (encryption may be implemented in the future)
- Javascript anti-debugging
- Javascript anti-emulation
- Burp detection
- Redirect after data collection (it's the same method as the office uri in Exploitation)

# How to use

## Deploy in prod

Pr0filer is self-contained, the backend needs PHP and there are already `.htaccess` files meant to be run with Apache, that can be adapted to any webserver.

Steps:
- Clone the repository `git clone https://gitlab.com/brn1337/pr0filer.git`
- [OPTIONAL] Insert a working API key in `backend\api_key_example.php` and rename it `backend\api_key.php`
- [OPTIONAL] Check `debug` is disabled and `check_anomaly` is enabled, I may forget :P ([more info here](#for-developers))
- Test it visiting `frontend\index.html`, `backend\log.txt` should appear
- Obfuscate and replace the following files using https://obfuscator.io/
  - `backend\clean.js`
  - `backend\clean_ie.js`
- Test again to verify that obfuscation didn't break anything
- ???
- Profit

## For developers

Enable `debug` in the following files:
  - `backend\clean.js`
  - `backend\clean_ie.js`
  - `backend\provider.php`
  - `backend\receiver.php`

Disable `check_anomaly` in the following files:
  - `backend\clean.js`
  - `backend\clean_ie.js`
  - `backend\provider.php`

# Future improvements

- [ ] Add capabilities to identify more AV/EDR (browser plugins, blocked websites, ...)
- [ ] Encrypt communication between agent and C2

# Examples

Examples will not be updated to reflect the new features.

Following some output from a windows 10 vm running chrome:

```php
109.**REDACTED**
Sun, 19 Dec 2021 18:54:09 +0100
array (
  'av' => 
  array (
    'bitdefender' => 'Bitdefender Wallet detected',
  ),
  'gpu' => 
  array (
    'gpu' => 'ANGLE (Google, Vulkan 1.2.0 (SwiftShader Device (Subzero) (0x0000C0DE)), SwiftShader driver-5.0.0)',
    'vendor' => 'Google Inc. (Google)',
  ),
  'navigator' => 
  array (
    [...]
    'plugins' => 
    array (
      [...]
        'name' => 'Microsoft Edge PDF Viewer',
        'filename' => 'internal-pdf-viewer',
        'description' => 'Portable Document Format',
        'length' => 2,
      ),
      [...]
    'hardwareConcurrency' => 2,
    [...]
    'languages' => 
    array (
      0 => 'en-US',
      1 => 'en',
    ),
    [...]
    ),
    'userAgentData' => 
    [...]
          'brand' => 'Google Chrome',
          'version' => '96',
        ),
      ),
      'mobile' => false,
      'platform' => 'Windows',
    ),
  ),
  'web' => 
  array (
    4 => 
    array (
      0 => 'filehosting',
      1 => 1,
      2 => 'https://github.com',
    ),
    7 => 
    array (
      0 => 'mail',
      1 => 1,
      2 => 'https://www.gmx.com',
    ),
    16 => 
    array (
      0 => 'hacking',
      1 => 1,
      2 => 'https://www.exploit-db.com',
    ),
    [...]
  ),
)
```

Following some output from an Android device running Firefox:

```php
92.**REDACTED**
Sun, 19 Dec 2021 18:41:38 +0100
array (
  'navigator' => 
  array (
    [...]
    'hardwareConcurrency' => 8,
    'platform' => 'Linux aarch64',
    'userAgent' => 'Mozilla/5.0 (Android 8.0.0; Mobile; rv:95.0) Gecko/95.0 Firefox/95.0',
    [...]
  ),
  'location' => 'http://evilwebsite.com/pr0filer/frontend/',
  'screen' => 
  array (
    'height' => 640,
    'width' => 360,
    [...]
  ),
  'gpu' => 
  array (
    'gpu' => 'Mali-T880',
    'vendor' => 'ARM',
  ),
  'ip' => 
  array (
    0 => '10.**REDACTED**',
    1 => '92.**REDACTED**',
  ),
  'touch' => 
  array (
    'ontouchstart' => true,
    'TouchEvent' => true,
  ),
  'av' => 
  array (
    'bitdefender' => 'Nothing detected',
  ),
  'web' => 
  array (
 	[...]
    12 => 
    array (
      0 => 'filehosting',
      1 => 1,
      2 => 'https://mega.nz',
    ),
    13 => 
    array (
      0 => 'mail',
      1 => 1,
      2 => 'https://www.gmx.com',
    ),
    [...]
    16 => 
    array (
      0 => 'censorship',
      1 => 1,
      2 => 'https://wikileaks.org',
    ),
    17 => 
    array (
      0 => 'hacking',
      1 => 1,
      2 => 'https://www.exploit-db.com',
    ),
    [...]
  ),
)
```

# Authors and acknowledgment

Inspired by Cobal Strike "System Profiler". Other references/acknowledgment in the source code.

# License

Pr0filer is released under the [MIT License](https://opensource.org/licenses/MIT).
