<?php

// CONFIG
$debug = False;

// initiation
if ($debug === False) {
	error_reporting(0);
	ini_set('display_errors', 0);
} else {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}

function write_anomaly(){
	$file = "anomalies.txt";
	$server_data = array(str_repeat(str_repeat("#", 60).PHP_EOL, 3), $_SERVER['REMOTE_ADDR'], date(DATE_RFC2822), "");
	$out_data = PHP_EOL . PHP_EOL . implode(PHP_EOL, $server_data) . '$_SERVER export:' . PHP_EOL . var_export($_SERVER, true) . PHP_EOL . '$_REQUEST export:' . PHP_EOL . var_export($_REQUEST, true);
	file_put_contents($file, $out_data, FILE_APPEND | LOCK_EX);
}

if (!isset($_POST['csrf'])){
	write_anomaly();
} else {
	if ($_POST['csrf'] == "0") {
		write_anomaly();
	} else {

		if ($debug === True) {echo "Received data: ".$_POST['csrf'];}
		$enc_data0 = str_replace('-|', '=', $_POST['csrf']);
		$enc_data1 = str_replace('|', '', $enc_data0);
		$enc_data2 = str_rot13(strrev($enc_data1));
		$enc_data3 = base64_decode($enc_data2);
		if ($debug === True) {echo "Decoded data: ".enc_data3;}

		$client_data = json_decode($enc_data3, true); //true or false no big difference...

		$server_data = array(str_repeat(str_repeat("#", 60).PHP_EOL, 3), $_SERVER['REMOTE_ADDR'], date(DATE_RFC2822), "");

		$out_data = PHP_EOL . PHP_EOL . implode(PHP_EOL, $server_data) . var_export($client_data, true);

		$file = "log.txt";

		file_put_contents($file, $out_data, FILE_APPEND | LOCK_EX);
	}
}


?>