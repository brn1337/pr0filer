<?php

// CONFIG
$check_anomaly = True;
$debug = False;
$vpnapiio_apikey_file = 'api_key.php';
/* example file content
<?php
$API_KEY_VPNAPI_IO = "40c76a97366c8433b2c69028d6db1977";
?>
*/

// http headers
header("Content-Type: application/javascript");
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.

// initiation
if ($debug === False) {
	error_reporting(0);
	ini_set('display_errors', 0);
} else {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}
$anomalyDetection = False;


// check anomalies
if ($check_anomaly === True) {

	// Detect proxy headers
	$test_HTTP_proxy_headers = array(
		'HTTP_VIA',
		'VIA',
		'Proxy-Connection',
		'HTTP_X_FORWARDED_FOR',
		'HTTP_FORWARDED_FOR',
		'HTTP_X_FORWARDED',
		'HTTP_FORWARDED',
		'HTTP_CLIENT_IP',
		'HTTP_FORWARDED_FOR_IP',
		'X-PROXY-ID',
		'MT-PROXY-ID',
		'X-TINYPROXY',
		'X_FORWARDED_FOR',
		'FORWARDED_FOR',
		'X_FORWARDED',
		'FORWARDED',
		'CLIENT-IP',
		'CLIENT_IP',
		'PROXY-AGENT',
		'HTTP_X_CLUSTER_CLIENT_IP',
		'FORWARDED_FOR_IP',
		'HTTP_PROXY_CONNECTION'
	);
	if ($anomalyDetection === False) {
		foreach($test_HTTP_proxy_headers as $header){
			if (isset($_SERVER[$header]) && !empty($_SERVER[$header])) {
				$anomalyDetection = True;
			}
		}
	}

	// Detect tor exit node
	function IsTorExitPoint(){
	    if (gethostbyname(ReverseIPOctets($_SERVER['REMOTE_ADDR']).".".$_SERVER['SERVER_PORT'].".".ReverseIPOctets($_SERVER['SERVER_ADDR']).".ip-port.exitlist.torproject.org")=="127.0.0.2") {
	        $anomalyDetection = True;
	    }
	}
	function ReverseIPOctets($inputip){
	    $ipoc = explode(".",$inputip);
	    return $ipoc[3].".".$ipoc[2].".".$ipoc[1].".".$ipoc[0];
	}
	if ($anomalyDetection === False) {
		IsTorExitPoint();
	}

	// detect vpn/proxy/tor/relay using VPNAPI.IO
	// Free API keys has a limit of 1000/requests per a day
	if ($anomalyDetection === False && $_SERVER['REMOTE_ADDR'] != "127.0.0.1" && file_exists($vpnapiio_apikey_file)) {
		require_once $vpnapiio_apikey_file;
		$API_URL = 'https://vpnapi.io/api/' . $_SERVER['REMOTE_ADDR'] . '?key=' . $API_KEY_VPNAPI_IO;
		$response = file_get_contents($API_URL);
		$response = json_decode($response);
		if(
			$response->security->vpn ||
			$response->security->proxy ||
			$response->security->tor ||
			$response->security->relay
		) {
			$anomalyDetection = True;
		}
	}

}

function write_anomaly(){
	$file = "anomalies.txt";
	$server_data = array(str_repeat(str_repeat("#", 60).PHP_EOL, 3), $_SERVER['REMOTE_ADDR'], date(DATE_RFC2822), "");
	$out_data = PHP_EOL . PHP_EOL . implode(PHP_EOL, $server_data) . '$_SERVER export:' . PHP_EOL . var_export($_SERVER, true) . PHP_EOL . '$_REQUEST export:' . PHP_EOL . var_export($_REQUEST, true);
	file_put_contents($file, $out_data, FILE_APPEND | LOCK_EX);
}

if ($anomalyDetection === False) {
	$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
	if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
		echo file_get_contents('./jquery.min.js', FILE_USE_INCLUDE_PATH);
		echo file_get_contents('./clean_ie.js', FILE_USE_INCLUDE_PATH);
	} else{
		echo file_get_contents('./jquery.min.js', FILE_USE_INCLUDE_PATH);
		echo file_get_contents('./bundle.dev.js', FILE_USE_INCLUDE_PATH);
		echo file_get_contents('./clean.js', FILE_USE_INCLUDE_PATH);		
	}
} else {
	echo file_get_contents('./jquery.min.js', FILE_USE_INCLUDE_PATH);
	write_anomaly();
}


?>